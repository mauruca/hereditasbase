#!/bin/bash
# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

if [ -z "$1" ]
  then
    echo "./build.sh <pythonversion (3.8)> <os (alpine, slim, ubuntu)> <osversion (3.10, buster, bionic)>"
    exit 1
fi

if [ -z "$2" ]
  then
    echo "./build.sh <pythonversion (3.8)> <os (alpine, slim, ubuntu)> <osversion (3.10, buster, bionic)>"
    exit 1
fi

if [ -z "$3" ]
  then
    echo "./build.sh <pythonversion (3.8)> <os (alpine, slim, ubuntu)> <osversion (3.10, buster, bionic)>"
    exit 1
fi

version=$(cat python/$1/version)
tagversion="$1.$2.$3"

arq="-f python/$1/$2/$3/Dockerfile"

docker build $arq -t hereditas/baseserver:$tagversion .
docker build $arq -t hereditas/baseserver:$version .
docker images hereditas/baseserver