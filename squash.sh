#!/bin/bash
# Copyright (C) 2017 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

#docker save bcdf7359e029 | sudo ~/apps/docker-squash -t hereditas/baseserver:slim | docker load

docker save $1 > image.tar
sudo ~/apps/docker-squash -i image.tar -o squashed.tar -t $2
cat squashed.tar | docker load
sudo rm image.tar squashed.tar
docker images $3
